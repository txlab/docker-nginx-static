[![size](https://badgen.net/docker/size/tennox/nginx-static)](https://hub.docker.com/r/tennox/nginx-static)
[![last commit](https://badgen.net/gitlab/last-commit/txlab/docker-nginx-static)](https://gitlab.com/txlab/docker-nginx-static/activity)
[![upstream github](https://badgen.net/badge/icon/gitlab?icon=github&label=upstream)](https://github.com/flashspys/docker-nginx-static)

# Super Lightweight Nginx Image for static files

This image can only be used for static file serving but has with **4 MB size** less than 1/10 the size of the official nginx image.
The bare running container needs **~2 MB RAM**.

## [Auto-updating fork](https://gitlab.com/txlab/docker-nginx-static) of [flashspys/docker-nginx-static](https://github.com/flashspys/docker-nginx-static)

- [rebuilt every week](https://gitlab.com/txlab/docker-nginx-static/-/pipelines) via GitLab CI Schedule, [without caching](https://gitlab.com/txlab/docker-nginx-static/-/blob/main/.gitlab-ci.yml#L11) to fetch latest alpine packages
- renovate bot that [updates alpine image tag](https://gitlab.com/txlab/docker-nginx-static/-/merge_requests?scope=all&state=all&author_username=txlab_renovate)
- (no auto-update of nginx - [yet](https://gitlab.com/txlab/docker-nginx-static/-/issues/3))

## TL;DR Readme

This command exposes an nginx server on port 8080 which serves the folder /absolute/path/to/serve from the host:

```bash
docker run -v /absolute/path/to/serve:/static -p 8080:80 tennox/nginx-static
```

### SPA mode
forward all requests to not existing files to index.html - [see config](./nginx.vh.spa.conf)
```bash
docker run -v $PWD/dist:/static -p 8080:80 tennox/nginx-static:spa
```
### Example Dockerfile
```dockerfile
FROM tennox/nginx-static # :spa if you need
COPY ./dist/ /static/
```

**More docs [upstream](https://github.com/flashspys/docker-nginx-static)**
